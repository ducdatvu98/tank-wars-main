using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpawn : MonoBehaviour
{
    public GameObject[] objectsToSpawn; // Mảng chứa các đối tượng sẽ được sinh ra
    public float minSpawnInterval; // Khoảng thời gian tối thiểu giữa các lần sinh ra
    public float maxSpawnInterval; // Khoảng thời gian tối đa giữa các lần sinh ra
    public Transform spawnPoint; // Vị trí spawn

    private float spawnTimer = 0.0f;
    private float currentSpawnInterval = 0.0f;

    void Start()
    {

    }

    void Update()
    {
        spawnTimer += Time.deltaTime;
        if (spawnTimer >= currentSpawnInterval)
        {
            SpawnObject();

            // Lấy ngẫu nhiên khoảng thời gian giữa các lần spawn tiếp theo
            currentSpawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
            spawnTimer = 0.0f;
        }
    }

    void SpawnObject()
    {
        // Random một đối tượng từ mảng objectsToSpawn
        int randomIndex = Random.Range(0, objectsToSpawn.Length);
        GameObject objectToSpawn = objectsToSpawn[randomIndex];
        Instantiate(objectToSpawn, spawnPoint.position, spawnPoint.rotation);
    }
}
