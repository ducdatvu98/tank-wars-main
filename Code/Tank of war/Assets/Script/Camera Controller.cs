using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float swipeSpeed = 2f; // Tốc độ di chuyển của camera khi vuốt
    public float maxMoveDistance = 5f; // Giới hạn độ dài di chuyển tối đa của camera
    private Vector3 startPosition; // Vị trí ban đầu của camera
    private Coroutine moveCoroutine; // Coroutine để di chuyển camera
    void Start()
    {
      startPosition = transform.position; // Lưu vị trí ban đầu của camera  
    }

    // Update is called once per frame
    void Update()
    {
      // Kiểm tra xem người dùng có vuốt màn hình không
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Lấy giá trị delta x của touch để xác định hướng di chuyển
            float touchDeltaX = Input.GetTouch(0).deltaPosition.x;

            // Tính toán vị trí mới của camera dựa trên delta x và tốc độ vuốt
            Vector3 newPosition = transform.position + Vector3.right * touchDeltaX * swipeSpeed * Time.deltaTime;

            // Giới hạn vị trí mới của camera trong khoảng maxMoveDistance
            newPosition.x = Mathf.Clamp(newPosition.x, startPosition.x - maxMoveDistance, startPosition.x + maxMoveDistance);

            // Gán vị trí mới cho camera
            transform.position = newPosition;
        }

        // Di chuyển sang trái nếu người chơi ấn mũi tên trái
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StartMoveCamera(-1);
        }
        // Di chuyển sang phải nếu người chơi ấn mũi tên phải
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StartMoveCamera(1);
        }

        // Dừng di chuyển khi người chơi nhả nút
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            StopMoveCamera();
        }
    }


    // Phương thức để bắt đầu di chuyển camera
    void StartMoveCamera(int direction)
    {
        // Kiểm tra xem di chuyển trước đó đã kết thúc chưa
        if (moveCoroutine != null)
        {
            StopCoroutine(moveCoroutine);
        }
        // Bắt đầu coroutine để di chuyển camera
        moveCoroutine = StartCoroutine(MoveCamera(direction));
    }

    // Phương thức để dừng di chuyển camera
    void StopMoveCamera()
    {
        if (moveCoroutine != null)
        {
            StopCoroutine(moveCoroutine);
            moveCoroutine = null;
        }
    }

    // Coroutine để di chuyển camera
    IEnumerator MoveCamera(int direction)
    {
        Vector3 targetPosition = transform.position + Vector3.right * direction * (maxMoveDistance+5f);
        targetPosition.x = Mathf.Clamp(targetPosition.x, startPosition.x - (maxMoveDistance+5f), startPosition.x + (maxMoveDistance+5f));
        
        while (transform.position != targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, 20f * Time.deltaTime);
            yield return null;
        }
    }
}
