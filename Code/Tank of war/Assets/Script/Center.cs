using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Center : MonoBehaviour
{
    public float health;         // máu bắt đầu
    private string enemyBulletTag = "B1"; // tag đạn đối phương
    public Image healthBar;   //thanh máu
    private float healthMax; // lưu lại máu tối đa
    public GameObject endPanel;
    public TMP_Text endText;
  
    void Start()
    {
        _SetAttackTag();
        healthMax = health; 
    }
    void FixedUpdate()
    {
        _healthChange();
        _endGame();              
    }
    void OnTriggerEnter(Collider Bullet) // trừ máu bằng pow của viên đạn va chạm
    {       
        if(Bullet.gameObject.tag == enemyBulletTag)
        {   if (healthBar != null)
            {
            healthBar.gameObject.SetActive(true);
            }
            health -= Bullet.gameObject.GetComponent<Bulletcontroller>().pow;          
        }      
    }
    void _healthChange()// điều khiển thanh máu và hoạt động khi hết máu
    {          
        if(healthBar != null)
        {
            healthBar.fillAmount = health/healthMax;   
        }
    }
    void _endGame()
    {
        if(health <=0)
        {   
            if(gameObject.tag == "B")
            {
                endText.text = "Noob";
            }
            endPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
        void _SetAttackTag() // chọn đối tượng chịu sát thương
    {
        if (gameObject.name == "B Center")
        {
        enemyBulletTag = "A1";
        }
    }
}
