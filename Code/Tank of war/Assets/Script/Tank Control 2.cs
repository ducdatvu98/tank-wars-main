using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Security;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;
public class TankControl2 : MonoBehaviour
{
    public float moveSpeed = 1f; // tốc độ di chuyển
    public float health;         // máu bắt đầu
    public int power;            // sức tấn công của đạn bắn ra
    public byte attackRange;     //tầm bắn
    private string attackTag = "B";   // tag đối tượng địch
    private string enemyBulletTag = "B1"; // tag đạn đối phương
    private string BulletTag = "A1"; // tag đạn spawn
    public string centerSpawn = "A center"; // Center sinh ra sẽ không check Raycast
    private float enemyDistance = Mathf.Infinity;// khoảng cách địch
    private Animator tank_Animator;  // Get Animator controller
    public Image healthBar;   //thanh máu
    private float healthMax; // lưu lại máu tối đa
    public GameObject bulletPrefab;  // Prefab của viên đạn
    public Transform spawnPoint;     // Vị trí xuất hiện của viên đạn
    private bool shouldShoot = true; // Biến kiểm tra xem có nên bắn viên đạn không
    private RaycastHit hit;          // tạo tia kiểm tra phía trước
    public Transform raycastPoint;   // điểm bắt đầu tia 
    private bool checkMove = false;  // quyền kiểm tra di chuyển
    private bool checkOverlap = false;// quyền kiểm tra trùng lặp
    void Start()
    {
        tank_Animator = GetComponent<Animator>();
        _SetAttackTag();
        healthMax = health;
        StartCoroutine(_checkMove());
    }
    void FixedUpdate()
    {   
        _enemyDistance(attackTag);
        _healthChange();
    }
    void Update()
    {
        _Attack();
        _AniMove();
    }
    void OnTriggerEnter(Collider Bullet) // trừ máu bằng pow của viên đạn va chạm
    {
        if (enemyBulletTag == Bullet.gameObject.tag)
        {
            if (healthBar != null)
            {
                healthBar.gameObject.SetActive(true);
            }
            health -= Bullet.gameObject.GetComponent<Bulletcontroller>().pow;
        }
    }
    void OnTriggerStay(Collider tankAlly) // kiểm tra trùng lặp, nếu có lùi 0,1f
    {
        if (tankAlly.gameObject.tag == gameObject.tag && checkOverlap)
        {
            transform.Translate(0.0f, 0.0f, -0.1f);
            checkOverlap = false;
        }
    }

    // Hoạt động di chuyển -----------------------------------------------------
    void _enemyDistance(string attackTag)// Tìm địch
    {
        GameObject[] allEnemies = GameObject.FindGameObjectsWithTag(this.attackTag); // Tìm
        GameObject minEnemy = null; // biến lưu kẻ địch gần nhất
        float minDistance = Mathf.Infinity; // lưu khoảng cách tạm thời
        foreach (GameObject enemy in allEnemies)
        {
            float distance = Vector3.Distance(enemy.transform.position, transform.position);
            if (distance < minDistance)
            {
                minEnemy = enemy;
                minDistance = distance;
                enemyDistance = distance; // Gán giá trị khoảng cách đến kẻ địch gần nhất
            }
        }

        if (allEnemies.Length == 0)
        {
            enemyDistance = 9999; // Gán giá trị enemyDistance = 9999 nếu không tìm thấy đối tượng nào
        }
    }
    IEnumerator _checkMove() // lặp lại kiểm tra trùng lặp theo các mốc thời gian
    {
        while (true)
        {
            checkOverlap = true;// cho phép kiểm tra chồng chéo để lùi lại
            yield return new WaitForSeconds(0.13257f); // sau khoảng thời gian này kiểm ra phía trước      
            if (Physics.Raycast(raycastPoint.position, transform.forward, out hit, 9f) && hit.collider.name != centerSpawn)
            {
                checkMove = false;
            }
            else
            {
                checkMove = true;
            }
            yield return new WaitForSeconds(0.12357f); // sau khoảng thời gian này lặp lại kiểm tra
        }
    }
    void _AniMove()// hoạt ảnh di chuyển
    {
        if (tank_Animator != null)
        {
            tank_Animator.SetTrigger("Move");
        }

        if (checkMove == false)
        {
            transform.Translate(Vector3.forward * (moveSpeed * 0) * Time.deltaTime);
        }
        if (checkMove == true)
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
    }

    //Hoạt động tấn công ------------------------------------------------------
    void _Attack() // Xác định Tấn công
    {
        if (attackRange >= enemyDistance && shouldShoot)
        {   
            checkOverlap=false;
            StartShootingSequence();
        }
        else
        {
            return;
        }
    }
    public void StartShootingSequence() // bắt đầu hoạt động bắn
    {
        StartCoroutine(ShootAfterDelay());
    }
    public void StopShootingSequence() // ngừng hoạt động bắn
    {
        shouldShoot = false;
        StopCoroutine(ShootAfterDelay());
    }
    IEnumerator ShootAfterDelay()// hoạt động bắn
    {
        if (shouldShoot)
        {
            shouldShoot = false;
            _AniAttack();
            yield return new WaitForSeconds(0.6f);// đợi thời điểm Animation spawn đạn
            _spawnBullet();
            yield return new WaitForSeconds(0.6f);// đợi hoàn thành animation bắn
            if (tank_Animator != null)
            {
                tank_Animator.SetTrigger("Move");
            }
            yield return new WaitForSeconds(0.2f);// đợi bắn viên đạn tiếp theo
            shouldShoot = true;
        }
    }
    void _spawnBullet()// tạo viên đạn
    {
        if (bulletPrefab != null && spawnPoint != null)
        {
            GameObject newBullet = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation);
            newBullet.tag = BulletTag;
            newBullet.gameObject.GetComponent<Bulletcontroller>().pow = power;
        }
    }
    void _SetAttackTag() // chọn đối tượng tấn công
    {
        if (gameObject.tag == "B")
        {
            attackTag = "A";
            enemyBulletTag = "A1";
            BulletTag = "B1";
            centerSpawn = "B Center";
        }
    }
    void _AniAttack()// Hoạt ảnh tấn công
    {
        if (tank_Animator != null && health > 0)
        {
            tank_Animator.SetTrigger("Attack");
        }
    }

    // Hoạt động về máu ------------------------------------------------------
    void _healthChange()// điều khiển thanh máu và hoạt động khi hết máu
    {
        if (healthBar != null)
        {
            healthBar.fillAmount = health / healthMax;
        }
        if (health <= 0 && !gameObject.CompareTag("Die"))
        {
            _AniDie();
        }
    }
    void _AniDie() // phá hủy khi hết máu
    {
        StopShootingSequence();// dừng hoạt động bắn dở
        StopCoroutine(_checkMove());
        gameObject.tag = "Die"; // đổi tag để không được chọn làm mục tiêu
        gameObject.GetComponent<BoxCollider>().enabled = false; // vô hiệu quá Collider
        moveSpeed = 0;
        if (tank_Animator != null)
        {
            tank_Animator.Play("Armature|B2 Die");
        }
        StartCoroutine(_destroy2s());
    }
    IEnumerator _destroy2s() // biến mất sau 2f rồi cộng tiền thưởng
    {
        yield return new WaitForSeconds(0.5f);
        tank_Animator.enabled = false;
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        if (gameObject.name == "A1(Clone)")
        {
            GameControler.moneyCount += 25;
        }
        if (gameObject.name == "A2(Clone)")
        {
            GameControler.moneyCount += 35;
        }

    }
}
