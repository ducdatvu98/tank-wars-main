using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float swipeSpeed = 2f; // Tốc độ di chuyển của camera khi vuốt
    public float maxMoveDistance = 5f; // Giới hạn độ dài di chuyển tối đa của camera
    private Vector3 startPosition; // Vị trí ban đầu của camera
    void Start()
    {
      startPosition = transform.position; // Lưu vị trí ban đầu của camera  
    }

    // Update is called once per frame
    void Update()
    {
      // Kiểm tra xem người dùng có vuốt màn hình không
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Lấy giá trị delta x của touch để xác định hướng di chuyển
            float touchDeltaX = Input.GetTouch(0).deltaPosition.x;

            // Tính toán vị trí mới của camera dựa trên delta x và tốc độ vuốt
            Vector3 newPosition = transform.position + Vector3.right * touchDeltaX * swipeSpeed * Time.deltaTime;

            // Giới hạn vị trí mới của camera trong khoảng maxMoveDistance
            newPosition.x = Mathf.Clamp(newPosition.x, startPosition.x - maxMoveDistance, startPosition.x + maxMoveDistance);

            // Gán vị trí mới cho camera
            transform.position = newPosition;
        }  
    }
}
