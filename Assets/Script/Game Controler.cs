using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class GameControler : MonoBehaviour
{   
    public AudioSource backMusic;
    public GameObject settingPanel;  // Menu setting
    public GameObject[] tankPrefab;    // Prefab xe tăng
    private List<int> spawnList = new();
    public Transform spawnPoint;     // Vị trí spawn xe tăng
    public int maxQueueSize = 5;     // Số lượng xe tăng tối đa trong hàng đợi
    private int tankQueueCountB2 = 0;  // Biến lưu trữ số lượng xe tăng còn lại trong hàng đợi
    private int tankQueueCountB1 = 0;
    public TMP_Text queueCountTextB2;  // Text hiển thị số lượng xe tăng còn lại trong hàng đợi
    public TMP_Text queueCountTextB1;
    public Image trainingImageB2;      // Hiệu ứng đợi training trên button
    public Image trainingImageB1;
    public float trainingTime = 4f;  // Thời gian đợi training
    private bool isSpawning = true;  // Kiểm tra xem có đang spawn xe tăng hay không
    public TMP_Text moneyText;
    public static int moneyCount = 150;
    public GameObject buttonMute;
    public GameObject buttonUnMute;

    void Start()
    {   
        if(backMusic != null)
        {
            backMusic.Play();
        }
    }
    void Update()
    {
        StartCoroutine(SpawnTankCoroutine());// spawn xe tăng
        _contMoney();
    }
    
    // Xử lý tiền kiếm được khi giết địch
    void _contMoney()// hiển thị số tiền đang có ra màn hình
    {
        moneyText.text = ("Money: " + moneyCount).ToString();
    }

    // Xử lý Spawn tank---------------------------------------------------------------------
    public void _ContSpawnB2() // thêm tank cần spawn vào hàng đợi B2
    {
        if (tankQueueCountB2 < maxQueueSize && moneyCount >= 30)
        {
            _AddSpawn(2);// thêm số 2 vào spawnList để đăng ký tank B2
            tankQueueCountB2++;
            UpdateCountTextTrain();
            moneyCount -= 30;// trừ đi giá xe tank
        }
    }
    public void _ContSpawnB1() // thêm tank cần spawn vào hàng đợi B1
    {
        if (tankQueueCountB1 < maxQueueSize && moneyCount >= 20)
        {
            _AddSpawn(1);// thêm số 1 vào spawnList để đăng ký tank B1
            tankQueueCountB1++;
            UpdateCountTextTrain();
            moneyCount -= 20; // trừ đi giá xe tank
        }
    }

    IEnumerator SpawnTankCoroutine()// sapwn tank trong hàng chờ
    {
        if (isSpawning && spawnList.Count > 0)
        {
            isSpawning = false;
            if (spawnList[0] == 2)
            {
                trainingImageB2.gameObject.SetActive(true);
                StartCoroutine(_TrainingLoadB2(trainingTime));
                yield return new WaitForSeconds(trainingTime);// Chờ thời gian đợi training
                Instantiate(tankPrefab[1], spawnPoint.position, spawnPoint.rotation);
                tankQueueCountB2--;// Giảm số lượng xe tăng trong B2
                UpdateCountTextTrain();
                trainingImageB2.gameObject.SetActive(false);
            }
            if (spawnList[0] == 1)
            {

                trainingImageB1.gameObject.SetActive(true);
                StartCoroutine(_TrainingLoadB1(trainingTime));
                yield return new WaitForSeconds(trainingTime);// Chờ thời gian đợi training
                Instantiate(tankPrefab[0], spawnPoint.position, spawnPoint.rotation);
                tankQueueCountB1--;// Giảm số lượng xe tăng trong B1
                UpdateCountTextTrain();
                trainingImageB1.gameObject.SetActive(false); 
            }
            spawnList.RemoveAt(0); // Xóa trong hàng đợi spawn 
            isSpawning = true;
        }
    }

    void UpdateCountTextTrain() // Cập nhật số tank cần sinh ra trên button
    {
        queueCountTextB2.text = tankQueueCountB2.ToString();
        queueCountTextB1.text = tankQueueCountB1.ToString();
    }
    IEnumerator _TrainingLoadB2(float trainingTime)// hiệu ứng tải training B2
    {
        float fillAmount = 1;
        trainingImageB2.fillAmount = fillAmount;
        while (fillAmount > 0f)
        {
            fillAmount -= Time.deltaTime / trainingTime;  
            trainingImageB2.fillAmount = fillAmount;
            yield return null;
        }
    }
    IEnumerator _TrainingLoadB1(float trainingTime)// hiệu ứng tải training B1
    {
        float fillAmount = 1;
        trainingImageB1.fillAmount = fillAmount;
        while (fillAmount > 0f)
        {
            fillAmount -= Time.deltaTime / trainingTime; 
            trainingImageB1.fillAmount = fillAmount;
            yield return null;
        }
    }
    void _AddSpawn(int number)// thêm vào danh sách Spawn
    {
        spawnList.Add(number);
    }

    // Các tính năng trên màn hình----------------------------------------------------------
    public void _menuSettingOn()
    {
        Time.timeScale = 0;
        settingPanel.SetActive(true);
    }
    public void _menuSettingOff()
    {
        settingPanel.SetActive(false);
        Time.timeScale = 1;
    }
    public void _RestartGame() // Load lại scene hiện tại
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        moneyCount = 150;
        Time.timeScale = 1;
    }
    public void _backMenu()
    {
        Debug.Log(" Menu đang phát triển");
    }
    public void _Mute()
    {   
        buttonUnMute.SetActive(true);
        buttonMute.SetActive(false);
        if (backMusic != null)
        {   
            backMusic.mute = true;
        }
    }
    public void _unMute()
    {
        buttonUnMute.SetActive(false);
        buttonMute.SetActive(true);
        if (backMusic != null)
        {
            backMusic.mute = false;
        }
    }
}
