using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleBackground : MonoBehaviour
{
     public GameObject backgroundObject;

    void Start()
    {
// Lấy kích thước màn hình
        Vector2 screenSize = new Vector2(Screen.width, Screen.height);

        // Tính toán tỷ lệ chiều rộng/chiều cao của màn hình
        float aspectRatio = screenSize.x / screenSize.y;

        // Tính toán chiều cao mới cho background dựa trên tỷ lệ chiều rộng/chiều cao của màn hình
        float newBackgroundHeight = backgroundObject.transform.localScale.x * aspectRatio;

        // Cập nhật chiều cao của background
        backgroundObject.transform.localScale = new Vector3(backgroundObject.transform.localScale.x, newBackgroundHeight, backgroundObject.transform.localScale.z);
    }

}
