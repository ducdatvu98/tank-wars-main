using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Security;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;
public class TankControl1 : MonoBehaviour
{
    public float moveSpeed = 1f; // tốc độ di chuyển
    public float health;         // máu bắt đầu
    public int power;            // sức tấn công của đạn bắn ra
    private string attackTag = "B";   // tag đối tượng địch
    private string enemyBulletTag = "B1"; // tag đạn đối phương
    private string BulletTag = "A1"; // tag đạn spawn
    public string centerSpawn = "A center"; // Center sinh ra sẽ không check Raycast
    private Animator tank_Animator;  // Get Animator controller
    public Image healthBar;   //thanh máu
    private float healthMax; // lưu lại máu tối đa
    public GameObject bulletPrefab;  // Prefab của viên đạn
    public Transform spawnPoint;     // Vị trí xuất hiện của viên đạn
    private bool shouldShoot = true; // Biến kiểm tra xem có nên bắn viên đạn không
    private RaycastHit hit;          // tạo tia kiểm tra phía trước
    public Transform raycastPoint;   // điểm bắt đầu tia Raycast
    private bool checkMove = false;  // quyền kiểm tra di chuyển
    private bool checkOverlap = false;// quyền kiểm tra trùng lặp
    void Start()
    {
        tank_Animator = GetComponent<Animator>();
        _SetAttackTag();
        healthMax = health;
        StartCoroutine(_checkMove());
    }
    void FixedUpdate()
    {   
        _healthChange();
        Debug.DrawRay(raycastPoint.position, transform.forward * 9f, Color.red);
    }
    void Update()
    {
        _checkAttack();
        _AniMove();
    }
    void OnTriggerEnter(Collider Bullet) // trừ máu bằng pow của viên đạn va chạm
    {
        if (enemyBulletTag == Bullet.gameObject.tag)
        {
            if (healthBar != null)
            {
                healthBar.gameObject.SetActive(true);
            }
            health -= Bullet.gameObject.GetComponent<Bulletcontroller>().pow;
        }
    }
    void OnTriggerStay(Collider tankAlly) // kiểm tra trùng lặp, nếu có lùi 0,1f
    {
        if (tankAlly.gameObject.tag == gameObject.tag && checkOverlap)
        {
            transform.Translate(0.0f, 0.0f, -0.1f);
            checkOverlap = false;
        }
    }

    // Hoạt động di chuyển -----------------------------------------------------
    IEnumerator _checkMove() // lặp lại kiểm tra trùng lặp theo các mốc thời gian
    {
        while (true)
        {   
            checkOverlap = true;// cho phép kiểm tra chồng chéo để lùi lại
            yield return new WaitForSeconds(0.13257f); // sau khoảng thời gian này kiểm ra phía trước      
            if (Physics.Raycast(raycastPoint.position, transform.forward, out hit, 9f) && hit.collider.name != centerSpawn)
            {
                checkMove = false;
            }
            else
            {
                checkMove = true;
            }
            yield return new WaitForSeconds(0.12357f); // sau khoảng thời gian này lặp lại kiểm tra
        }
    }
    void _AniMove()// hoạt ảnh di chuyển
    {
        if (checkMove == false)
        {
            transform.Translate(Vector3.forward * (moveSpeed * 0) * Time.deltaTime);
        }
        if (checkMove == true && health>0)
        {   
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            if (tank_Animator != null)
            {   
            tank_Animator.SetTrigger("Move");
            }           
        }
    }

    //Hoạt động tấn công ------------------------------------------------------
    void _checkAttack() // Xác định tấn công
    {   
        if (Physics.Raycast(raycastPoint.position, transform.forward, out hit, 9f) && hit.collider.tag == attackTag)
        {   
            checkOverlap=false;
            checkMove = false;
            if(shouldShoot)
            {          
            StartShootingSequence();          
            }
        }
        else
        {
            return;
        }
    }
    public void StartShootingSequence() // bắt đầu hoạt động tấn công
    {    
        StartCoroutine(ShootAfterDelay());
    }
    public void StopShootingSequence() // ngừng hoạt động tấn công
    {
        shouldShoot = false;
        StopCoroutine(ShootAfterDelay());
    }
    IEnumerator ShootAfterDelay()// hoạt động bắn
    {   
        if (shouldShoot)
        {   
            shouldShoot = false;
            _AniAttack();
            yield return new WaitForSeconds(0.77f);// đợi thời điểm Animation spawn đạn
            _spawnBullet();
            yield return new WaitForSeconds(0.5f);// đợi hoàn thành animation bắn
            if (tank_Animator != null && health>0)
            {
                tank_Animator.SetTrigger("Move");
            }
            yield return new WaitForSeconds(0.23f);// đợi bắn viên đạn tiếp theo
            shouldShoot = true;
            
        }
    }
    void _spawnBullet()// tạo viên đạn
    {
        if (bulletPrefab != null && spawnPoint != null)
        {
            GameObject newBullet = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation);
            newBullet.tag = BulletTag;
            newBullet.gameObject.GetComponent<Bulletcontroller>().pow = power;
        }
    }
    void _SetAttackTag() // chọn đối tượng tấn công
    {
        if (gameObject.tag == "B")
        {
            attackTag = "A";
            enemyBulletTag = "A1";
            BulletTag = "B1";
            centerSpawn = "B Center";
        }
    }
    void _AniAttack()// Hoạt ảnh tấn công
    {
        if (tank_Animator != null && health > 0)
        {
            tank_Animator.SetTrigger("Attack");
        }
    }

    // Hoạt động về máu ------------------------------------------------------
    void _healthChange()// điều khiển thanh máu và hoạt động khi hết máu
    {
        if (healthBar != null)
        {
            healthBar.fillAmount = health / healthMax;
        }
        if (health <= 0 && !gameObject.CompareTag("Die"))
        {
            _AniDie();
        }
    }
    void _AniDie() // phá hủy khi hết máu
    {
        StopShootingSequence();// dừng hoạt động bắn dở
        StopCoroutine(_checkMove());
        gameObject.tag = "Die"; // đổi tag để không bị chọn làm mục tiêu
        gameObject.GetComponent<BoxCollider>().enabled = false; // vô hiệu quá Collider
        moveSpeed = 0;
        if (tank_Animator != null)
        {
            tank_Animator.Play("B1|B1 Die");
        }
        StartCoroutine(_destroy2s());
    }
    IEnumerator _destroy2s() // biến mất sau 2f rồi cộng tiền thưởng
    {
        yield return new WaitForSeconds(0.5f);
        tank_Animator.enabled = false;
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        if (gameObject.name == "A1(Clone)")
        {
            GameControler.moneyCount += 25;
        }
        if (gameObject.name == "A2(Clone)")
        {
            GameControler.moneyCount += 35;
        }

    }
}
